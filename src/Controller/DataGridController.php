<?php

declare(strict_types=1);

namespace UXF\DataGrid\Controller;

use Symfony\Component\HttpFoundation\Response;
use UXF\Core\Attribute\FromQuery;
use UXF\Core\Contract\Autocomplete\AutocompleteFactory;
use UXF\Core\Contract\Autocomplete\AutocompleteResult;
use UXF\DataGrid\DataGridFactory;
use UXF\DataGrid\DataGridResponseProvider;
use UXF\DataGrid\Http\DataGridAutocompleteRequestQuery;
use UXF\DataGrid\Http\DataGridExportRequestQuery;
use UXF\DataGrid\Http\DataGridRequest;
use UXF\DataGrid\Http\DataGridResponse;
use UXF\DataGrid\Schema\DataGridSchema;

/**
 * @author Jakub Janata <jakubjanata@gmail.com>
 */
final readonly class DataGridController
{
    public function __construct(
        private DataGridFactory $dataGridFactory,
        private DataGridResponseProvider $responseProvider,
        private ?AutocompleteFactory $autocompleteFactory = null,
    ) {
    }

    public function getData(string $name, #[FromQuery] DataGridRequest $request): DataGridResponse
    {
        return $this->responseProvider->get($name, $request);
    }

    public function getExport(string $name, #[FromQuery] DataGridRequest $request, #[FromQuery] DataGridExportRequestQuery $query): Response
    {
        return $this->responseProvider->export($name, $request, [], $query->format);
    }

    public function getSchema(string $name): DataGridSchema
    {
        return $this->dataGridFactory->createSchema($name);
    }

    /**
     * @return AutocompleteResult[]
     */
    public function getAutocomplete(string $name, #[FromQuery] DataGridAutocompleteRequestQuery $query): array
    {
        return $this->autocompleteFactory?->createAutocomplete($name)->search($query->term, 20) ?? [];
    }
}
