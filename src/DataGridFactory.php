<?php

declare(strict_types=1);

namespace UXF\DataGrid;

use LogicException;
use Psr\Container\ContainerInterface;
use RuntimeException;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use UXF\DataGrid\Schema\DataGridSchema;

/**
 * @author Jakub Janata <jakubjanata@gmail.com>
 */
final readonly class DataGridFactory
{
    /**
     * @param DataGridBuilderFactory<mixed> $annotationDataGridBuilderFactory
     */
    public function __construct(
        private bool $schemaWithHiddenColumns,
        private ContainerInterface $container,
        private PropertyAccessorInterface $propertyAccessor,
        private DataGridBuilderFactory $annotationDataGridBuilderFactory,
    ) {
    }

    /**
     * @param mixed[] $options
     * @return DataGrid<mixed>
     */
    public function createDataGrid(string $name, array $options = []): DataGrid
    {
        $builder = $this->createBuilder($name, $options);

        $gridType = $this->findGridType($name);
        $dataSource = $gridType instanceof DataGridType
            ? $gridType->getDataSource($options)
            : $builder->getDataSource();

        if ($dataSource === null) {
            throw new RuntimeException("Please set DataSource for $name grid");
        }

        return new DataGrid(
            $this->propertyAccessor,
            $dataSource,
            $builder->getColumns(),
            $builder->getFilters(),
            $builder->getTabs(),
            $builder->getFullTextFilterCallback(),
            $builder->getRowContentCallback(),
            $builder->getRowExportCallback(),
            $builder->getDefaultSortColumnName(),
            $builder->getDefaultSortColumnDirection(),
            $builder->getDefaultPerPage(),
        );
    }

    /**
     * @param mixed[] $options
     */
    public function createSchema(string $name, array $options = []): DataGridSchema
    {
        return $this->createBuilder($name, $options)->createSchema($this->schemaWithHiddenColumns);
    }

    /**
     * @param mixed[] $options
     * @return DataGridBuilder<mixed>
     */
    private function createBuilder(string $name, array $options = []): DataGridBuilder
    {
        $builder = $this->annotationDataGridBuilderFactory->tryCreateBuilder($name) ?? new DataGridBuilder($name);

        $this->findGridType($name)?->buildGrid($builder, $options);

        return $builder;
    }

    /**
     * @return DataGridTypeWithoutCustomDataSource<mixed>|null
     */
    private function findGridType(string $name): ?DataGridTypeWithoutCustomDataSource
    {
        $name = "datagrid.$name";

        if ($this->container->has($name)) {
            $gridType = $this->container->get($name);
            if (!$gridType instanceof DataGridTypeWithoutCustomDataSource) {
                throw new LogicException("Invalid type $name");
            }

            return $gridType;
        }

        return null;
    }
}
