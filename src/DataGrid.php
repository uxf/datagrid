<?php

declare(strict_types=1);

namespace UXF\DataGrid;

use Closure;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use UXF\DataGrid\Column\Column;
use UXF\DataGrid\DataSource\DataSource;
use UXF\DataGrid\Filter\Filter;
use UXF\DataGrid\Http\DataGridRequest;
use UXF\DataGrid\Http\DataGridResponse;
use UXF\DataGrid\Http\FilterRequest;
use UXF\DataGrid\Tab\Tab;

/**
 * @template T
 */
final readonly class DataGrid
{
    /**
     * @param DataSource<T> $dataSource
     * @param array<string, Column<T>> $columns
     * @param array<string, Filter<mixed>> $filters
     * @param array<string, Tab> $tabs
     * @phpstan-param Closure(mixed $source, string $term): mixed|null $fullTextFilterCallback
     * @phpstan-param Closure(T $item): mixed|null $rowContentCallback
     * @phpstan-param Closure(T $item): mixed|null $rowExportCallback
     */
    public function __construct(
        private PropertyAccessorInterface $propertyAccessor,
        private DataSource $dataSource,
        private array $columns,
        private array $filters,
        private array $tabs,
        private ?Closure $fullTextFilterCallback,
        private ?Closure $rowContentCallback,
        private ?Closure $rowExportCallback,
        private string $defaultSortColumnName,
        private DataGridSortDir $defaultSortColumnDirection,
        private int $defaultPerPage,
    ) {
    }

    public function getResult(DataGridRequest $request): DataGridResponse
    {
        $this->dataSource->init();

        $tabFilterFn = $request->tab !== null && isset($this->tabs[$request->tab])
            ? $this->tabs[$request->tab]->filterCallback
            : null;

        $totalCount = $this->dataSource->getTotalCount($tabFilterFn);

        $tabCounts = null;
        if ($request->withTabCounts === true) {
            $tabCounts = [];
            foreach ($this->tabs as $name => $tab) {
                $tabCounts[$name] = $this->dataSource->getTotalCount($tab->filterCallback);
            }
        }

        $this->applySortAndFilter($request);

        $filteredCount = $this->dataSource->getFilteredCount();

        // limit and offset
        $limit = max($request->perPage ?? $this->defaultPerPage, 0);
        $this->dataSource->applyLimitAndOffset($limit, max($request->page ?? 0, 0) * $limit);

        // process data
        $result = [];
        foreach ($this->dataSource->getData() as $item) {
            if (is_callable($this->rowContentCallback)) {
                $result[] = call_user_func($this->rowContentCallback, $item);
                continue;
            }

            $row = [];
            foreach ($this->columns as $column) {
                $row[$column->getName()] = $column->getContent($item, $this->propertyAccessor);
            }
            $result[] = $row;
        }

        return new DataGridResponse($result, $filteredCount, $totalCount, $tabCounts);
    }

    /**
     * @return mixed[]
     */
    public function getExport(DataGridRequest $request): array
    {
        $this->dataSource->init();

        $this->applySortAndFilter($request);

        // process data
        $result = [];
        foreach ($this->dataSource->getData() as $item) {
            if (is_callable($this->rowExportCallback)) {
                $result[] = call_user_func($this->rowExportCallback, $item);
                continue;
            }

            $row = [];
            foreach ($this->columns as $column) {
                if (!$column->isExport()) {
                    continue;
                }

                $row[$column->getLabel()] = $column->getExportContent($item, $this->propertyAccessor);
            }
            $result[] = $row;
        }

        return $result;
    }

    private function applySortAndFilter(DataGridRequest $request): void
    {
        // sort
        if ($request->s !== null) {
            $sortDir = $request->s->dir;
            $sortColumn = $this->columns[$request->s->name] ?? null;
        } else {
            $sortDir = $request->dir ?? $this->defaultSortColumnDirection;
            $sortColumn = $this->columns[$request->sort] ?? null;
        }

        $sortColumn ??= $this->columns[$this->defaultSortColumnName] ?? null;

        if ($sortColumn !== null) {
            $this->dataSource->applySort($sortColumn, $sortDir);
        } else {
            trigger_error("Grid '{$this->defaultSortColumnName}' column doesn't exist (it's required for default sort)", E_USER_WARNING);
        }

        $filterRequests = $request->f;

        // init default filters
        foreach ($this->filters as $filter) {
            $name = $filter->getName();
            $defaultValue = $filter->getDefaultValue();
            if (
                $defaultValue !== null &&
                array_filter($filterRequests, static fn (FilterRequest $r) => $r->name === $name) === []
            ) {
                $filterRequests[] = new FilterRequest($filter->getName(), $defaultValue);
            }
        }

        // tabs
        if ($request->tab !== null && isset($this->tabs[$request->tab])) {
            $fn = fn (mixed $qbCallback, mixed $value, ?string $operator) => call_user_func($this->tabs[$request->tab]->filterCallback, $qbCallback);

            $this->dataSource->applyFilter(
                new class($fn) extends Filter {
                    /** @param Closure(QueryBuilder $qbCallback, mixed $value, ?string $operator): mixed|null $qbCallback */
                    public function __construct(protected ?Closure $qbCallback)
                    {
                        parent::__construct('', '');
                    }

                    public function getDefaultType(): string
                    {
                        return '';
                    }

                    public function mapFilterValue(mixed $value): mixed
                    {
                        return $value;
                    }
                },
                0, // fake value -> cannot be null or []
                null,
            );
        }

        // filters
        foreach ($filterRequests as $filterRequest) {
            $filter = $this->filters[$filterRequest->name] ?? null;
            if ($filter !== null) {
                $this->dataSource->applyFilter($filter, $filterRequest->value, $filterRequest->op);
            }
        }

        if ($this->fullTextFilterCallback !== null && $request->search !== null) {
            $this->dataSource->applyFullTextFilter($this->fullTextFilterCallback, $request->search);
        }
    }
}
