<?php

declare(strict_types=1);

namespace UXF\DataGrid\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use UXF\DataGrid\DataGridTypeWithoutCustomDataSource;

/**
 * @author Jakub Janata <jakubjanata@gmail.com>
 */
final readonly class DataGridTypePass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container): void
    {
        foreach ($container->findTaggedServiceIds('uxf.datagrid.type') as $id => $tag) {
            $definition = $container->getDefinition($id)
                ->setPublic(true)
                ->setAutowired(true);

            $class = $definition->getClass();
            assert(is_string($class) && is_a($class, DataGridTypeWithoutCustomDataSource::class, true));

            $container->setAlias('datagrid.' . $class::getName(), $id)->setPublic(true);
        }
    }
}
