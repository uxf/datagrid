<?php

declare(strict_types=1);

namespace UXF\DataGrid\Export;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Serializer\SerializerInterface;
use UXF\Core\Exception\BasicException;
use XLSXWriter;

final readonly class DataGridExporter
{
    public function __construct(
        private SerializerInterface $serializer,
    ) {
    }

    /**
     * @param mixed[] $data
     */
    public function export(array $data, string $format): Response
    {
        return match ($format) {
            'csv' => $this->exportCsv($data),
            'xlsx' => $this->exportXlsx($data),
            default => throw BasicException::badRequest(),
        };
    }

    /**
     * @param mixed[] $data
     */
    private function exportCsv(array $data): Response
    {
        $response = new StreamedResponse(fn () => file_put_contents('php://output', chr(239) . chr(187) . chr(191) . $this->serializer->serialize($data, 'csv')));
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', 'attachment; filename="export.csv"');
        return $response;
    }

    /**
     * Experimental!!!
     * @param mixed[] $data
     */
    private function exportXlsx(array $data): Response
    {
        $writer = new XLSXWriter();
        $writer->writeSheet($data, header_types: array_map(static fn () => 'string', $data[0] ?? []));

        $response = new StreamedResponse(fn () => $writer->writeToStdOut());
        $response->headers->set('Content-Type:', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $response->headers->set('Content-Disposition', 'attachment; filename="export.xlsx"');
        return $response;
    }
}
