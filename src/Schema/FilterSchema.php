<?php

declare(strict_types=1);

namespace UXF\DataGrid\Schema;

final readonly class FilterSchema
{
    /**
     * @param FilterOption[]|null $options
     */
    public function __construct(
        public string $name,
        public string $label,
        public string $type,
        public ?string $autocomplete,
        public ?array $options,
    ) {
    }
}
