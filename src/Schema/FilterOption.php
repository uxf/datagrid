<?php

declare(strict_types=1);

namespace UXF\DataGrid\Schema;

final readonly class FilterOption
{
    public function __construct(
        public mixed $id,
        public string $label,
    ) {
    }
}
