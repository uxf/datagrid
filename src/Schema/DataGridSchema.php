<?php

declare(strict_types=1);

namespace UXF\DataGrid\Schema;

use UXF\DataGrid\DataGridSort;
use UXF\DataGrid\DataGridSortDir;

final readonly class DataGridSchema
{
    /**
     * @param ColumnSchema[] $columns
     * @param FilterSchema[] $filters
     * @param TabSchema[] $tabs
     */
    public function __construct(
        public string $name,
        public array $columns,
        public array $filters,
        public array $tabs,
        public bool $fullText,
        public DataGridSort $s,
        public int $perPage,
        /** @deprecated use $s */
        public string $sort,
        /** @deprecated use $s */
        public DataGridSortDir $dir,
    ) {
    }
}
