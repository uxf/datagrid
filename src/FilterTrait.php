<?php

declare(strict_types=1);

namespace UXF\DataGrid;

use BackedEnum;
use Closure;
use Doctrine\ORM\QueryBuilder;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Decimal;
use UXF\Core\Utils\SH;
use UXF\DataGrid\Filter\CheckboxFilter;
use UXF\DataGrid\Filter\DateRangeFilter;
use UXF\DataGrid\Filter\DateTimeRangeFilter;
use UXF\DataGrid\Filter\DecimalRangeFilter;
use UXF\DataGrid\Filter\EntityMultiSelectFilter;
use UXF\DataGrid\Filter\EntitySelectFilter;
use UXF\DataGrid\Filter\EnumFilter;
use UXF\DataGrid\Filter\EnumsFilter;
use UXF\DataGrid\Filter\IntRangeFilter;
use UXF\DataGrid\Filter\SelectFilter;
use UXF\DataGrid\Filter\StringFilter;
use UXF\DataGrid\Schema\FilterOption;

trait FilterTrait
{
    public static function id(): StringFilter
    {
        return new StringFilter('id', 'ID');
    }

    /**
     * @phpstan-param Closure(QueryBuilder $qb, bool $value): mixed|null $fn
     */
    public static function checkbox(string $name, string $label, ?Closure $fn = null): CheckboxFilter
    {
        $filter = new CheckboxFilter($name, $label);

        if ($fn instanceof Closure) {
            $filter->setQbCallback($fn);
        }
        return $filter;
    }

    /**
     * @phpstan-param Closure(QueryBuilder $qb, array{Date|null, Date|null} $value): mixed|null $fn
     */
    public static function date(string $name, string $label, ?Closure $fn = null): DateRangeFilter
    {
        $filter = new DateRangeFilter($name, $label);

        if ($fn instanceof Closure) {
            $filter->setQbCallback($fn);
        }
        return $filter;
    }

    /**
     * @phpstan-param Closure(QueryBuilder $qb, array{DateTime|null, DateTime|null} $value): mixed|null $fn
     */
    public static function dateTime(string $name, string $label, ?Closure $fn = null): DateTimeRangeFilter
    {
        $filter = new DateTimeRangeFilter($name, $label);

        if ($fn instanceof Closure) {
            $filter->setQbCallback($fn);
        }
        return $filter;
    }

    /**
     * @phpstan-param Closure(QueryBuilder $qb, array{int|null, int|null} $value): mixed|null $fn
     */
    public static function intRange(string $name, string $label, ?Closure $fn = null): IntRangeFilter
    {
        $filter = new IntRangeFilter($name, $label);

        if ($fn instanceof Closure) {
            $filter->setQbCallback($fn);
        }
        return $filter;
    }


    /**
     * @phpstan-param Closure(QueryBuilder $qb, array{Decimal|null, Decimal|null} $value): mixed|null $fn
     */
    public static function decimalRange(string $name, string $label, ?Closure $fn = null): DecimalRangeFilter
    {
        $filter = new DecimalRangeFilter($name, $label);

        if ($fn instanceof Closure) {
            $filter->setQbCallback($fn);
        }
        return $filter;
    }

    /**
     * @phpstan-param Closure(QueryBuilder $qb, int|string $value): mixed|null $fn
     */
    public static function entity(string $name, string $label, string $autocomplete, ?Closure $fn = null): EntitySelectFilter
    {
        $filter = new EntitySelectFilter($name, $label, $autocomplete);

        if ($fn instanceof Closure) {
            $filter->setQbCallback($fn);
        }
        return $filter;
    }

    /**
     * @phpstan-param Closure(QueryBuilder $qb, array<string|int> $value): mixed|null $fn
     */
    public static function entities(string $name, string $label, string $autocomplete, ?Closure $fn = null): EntityMultiSelectFilter
    {
        $filter = new EntityMultiSelectFilter($name, $label, $autocomplete);

        if ($fn instanceof Closure) {
            $filter->setQbCallback($fn);
        }
        return $filter;
    }

    /**
     * @phpstan-param Closure(QueryBuilder $qb, string $value): mixed|null $fn
     */
    public static function string(string $name, string $label, ?Closure $fn = null): StringFilter
    {
        $filter = new StringFilter($name, $label);
        if ($fn instanceof Closure) {
            $filter->setQbCallback($fn);
        }
        return $filter;
    }

    /**
     * @template T of BackedEnum
     * @param class-string<T> $enumClass
     * @phpstan-param Closure(QueryBuilder $qb, T $value): mixed|null $fn
     * @return EnumFilter<T>
     */
    public static function enum(string $name, string $label, string $enumClass, ?Closure $fn = null): EnumFilter
    {
        $filter = new EnumFilter($name, $label, $enumClass);

        if ($fn instanceof Closure) {
            $filter->setQbCallback($fn);
        }
        return $filter;
    }

    /**
     * @template T of BackedEnum
     * @param class-string<T> $enumClass
     * @phpstan-param Closure(QueryBuilder $qb, T[] $value): mixed|null $fn
     * @return EnumsFilter<T>
     */
    public static function enums(string $name, string $label, string $enumClass, ?Closure $fn = null): EnumsFilter
    {
        $filter = new EnumsFilter($name, $label, $enumClass);

        if ($fn instanceof Closure) {
            $filter->setQbCallback($fn);
        }
        return $filter;
    }

    /**
     * @phpstan-param Closure(QueryBuilder $qb, bool $value): mixed|null $fn
     */
    public static function boolean(string $name, string $label, ?Closure $fn = null): SelectFilter
    {
        $filter = new SelectFilter($name, $label, [
            new FilterOption(1, 'ANO'),
            new FilterOption(0, 'NE'),
        ]);

        if ($fn instanceof Closure) {
            $filter->setQbCallback(fn (QueryBuilder $qb, string|int $value) => $fn($qb, SH::s2b((string) $value)));
        }
        return $filter;
    }

    /**
     * @param FilterOption[] $options
     * @phpstan-param Closure(QueryBuilder $qb, int|string $value): mixed|null $fn
     */
    public static function select(string $name, string $label, array $options, ?Closure $fn = null): SelectFilter
    {
        $filter = new SelectFilter($name, $label, $options);

        if ($fn instanceof Closure) {
            $filter->setQbCallback($fn);
        }
        return $filter;
    }
}
