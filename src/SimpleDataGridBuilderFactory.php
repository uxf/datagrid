<?php

declare(strict_types=1);

namespace UXF\DataGrid;

/**
 * @template-implements DataGridBuilderFactory<mixed>
 */
final readonly class SimpleDataGridBuilderFactory implements DataGridBuilderFactory
{
    /**
     * @return DataGridBuilder<mixed>
     */
    public function tryCreateBuilder(string $name): DataGridBuilder
    {
        return new DataGridBuilder($name);
    }
}
