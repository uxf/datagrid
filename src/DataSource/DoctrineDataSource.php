<?php

declare(strict_types=1);

namespace UXF\DataGrid\DataSource;

use Closure;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Throwable;
use UXF\DataGrid\Column\Column;
use UXF\DataGrid\DataGridSortDir;
use UXF\DataGrid\Filter\Filter;
use UXF\DataGrid\Filter\RangeFilter;
use UXF\DataGrid\Filter\StringFilter;
use UXF\DataGrid\Utils\DataGridMagicHelper;

/**
 * @template T
 * @template-implements DataSource<T>
 */
final class DoctrineDataSource implements DataSource
{
    private string $rootAlias;
    private int $paramCounter = 0;

    public function __construct(
        private readonly QueryBuilder $queryBuilder,
        private readonly bool $withoutPaginator = false,
        private readonly bool $withoutDistinct = false,
        ?string $rootAlias = null,
    ) {
        $this->rootAlias = $rootAlias ?? $this->queryBuilder->getRootAliases()[0];
    }

    public function init(): void
    {
    }

    public function getTotalCount(?Closure $callback = null): int
    {
        $qb = (clone $this->queryBuilder)
            ->select("COUNT({$this->getDistinct()} $this->rootAlias.id)")
            ->resetDQLPart('orderBy');

        if ($callback !== null) {
            $callback($qb);
        }

        return (int) $qb->getQuery()->getSingleScalarResult();
    }

    public function getFilteredCount(): int
    {
        if ($this->withoutPaginator) {
            return (int) (clone $this->queryBuilder)
                ->select("COUNT({$this->getDistinct()} $this->rootAlias.id)")
                ->resetDQLPart('orderBy')
                ->getQuery()
                ->getSingleScalarResult();
        }

        return (new Paginator($this->queryBuilder))->count();
    }

    public function applySort(Column $column, DataGridSortDir $direction): void
    {
        $paths = $column->getSortColumnPaths();

        if (!$column->isRawSort()) {
            $paths = $column->isEmbedded()
                ? array_map(fn (string $path) => "$this->rootAlias.{$path}", $paths)
                : array_map(fn (string $path) => $this->resolveColumnPath($path), $paths);
        }

        foreach ($paths as $path) {
            $this->queryBuilder->addOrderBy($path, $direction->value);
        }
    }

    public function applyFilter(Filter $filter, mixed $value, ?string $operand): void
    {
        if ($value === null) {
            return;
        }

        try {
            $value = $filter->mapFilterValue($value);
        } catch (Throwable $e) {
            trigger_error('Invalid type: ' . $e->getMessage(), E_USER_WARNING);
            return;
        }

        if ($value === [] || $value === '' || $value === null) {
            return;
        }

        if ($filter->getQbCallback() !== null) {
            $filter->getQbCallback()($this->queryBuilder, $value, $operand);
            return;
        }

        /** @var literal-string $columnPath */
        $columnPath = $filter->isEmbedded()
            ? "$this->rootAlias.{$filter->getColumnPath()}"
            : $this->resolveColumnPath($filter->getColumnPath());

        if ($filter instanceof RangeFilter) {
            [$from, $to] = $value;

            if ($from !== null) {
                $this->queryBuilder
                    ->andWhere("$columnPath >= ?$this->paramCounter")
                    ->setParameter($this->paramCounter++, $from);
            }
            if ($to !== null) {
                $this->queryBuilder
                    ->andWhere("$columnPath <= ?$this->paramCounter")
                    ->setParameter($this->paramCounter++, $to);
            }
        } elseif ($filter instanceof StringFilter) {
            [$condition, $param] = match ($filter->getMatchType()) {
                "equals" => ["CAST($columnPath AS text) = ?$this->paramCounter", $value],
                "startsWith" => ["UNACCENT(CAST($columnPath AS text)) LIKE UNACCENT(?$this->paramCounter)", "$value%"],
                "endsWith" => ["UNACCENT(CAST($columnPath AS text)) LIKE UNACCENT(?$this->paramCounter)", "%$value"],
                default => ["UNACCENT(CAST($columnPath AS text)) LIKE UNACCENT(?$this->paramCounter)", "%$value%"],
            };

            $this->queryBuilder
                ->andWhere($condition)
                ->setParameter($this->paramCounter++, $param);
        } else {
            $this->queryBuilder
                ->andWhere("$columnPath IN (?$this->paramCounter)")
                ->setParameter($this->paramCounter++, $value);
        }
    }

    public function applyFullTextFilter(Closure $callback, string $value): void
    {
        $callback($this->queryBuilder, $value);
    }

    public function applyLimitAndOffset(int $limit, int $offset): void
    {
        $this->queryBuilder
            ->setFirstResult($offset)
            ->setMaxResults($limit);
    }

    /**
     * {@inheritdoc}
     */
    public function getData(): array
    {
        if ($this->withoutPaginator) {
            return $this->queryBuilder->getQuery()->getResult();
        }

        return iterator_to_array(new Paginator($this->queryBuilder));
    }

    private function resolveColumnPath(string $columnPath): string
    {
        if (!str_contains($columnPath, '.')) {
            return "$this->rootAlias.{$columnPath}";
        }

        // add to join
        [$entity] = explode('.', $columnPath);

        /** @var literal-string $joinString */
        $joinString = "$this->rootAlias.$entity";
        DataGridMagicHelper::trySafelyApplyJoin($this->queryBuilder, $joinString);

        return $columnPath;
    }

    private function getDistinct(): string
    {
        return $this->withoutDistinct ? '' : 'DISTINCT';
    }
}
