<?php

declare(strict_types=1);

namespace UXF\DataGrid\DataSource;

use Closure;
use UXF\DataGrid\Column\Column;
use UXF\DataGrid\DataGridSortDir;
use UXF\DataGrid\Filter\Filter;

/**
 * @template T
 */
interface DataSource
{
    public function init(): void;

    /**
     * @param Closure(mixed $callback): mixed|null $callback
     */
    public function getTotalCount(?Closure $callback = null): int;

    /**
     * @param Column<T> $column
     */
    public function applySort(Column $column, DataGridSortDir $direction): void;

    /**
     * @param Filter<mixed> $filter
     */
    public function applyFilter(Filter $filter, mixed $value, ?string $operand): void;

    /**
     * @phpstan-param Closure(mixed $source, string $term): mixed $callback
     */
    public function applyFullTextFilter(Closure $callback, string $value): void;

    public function getFilteredCount(): int;

    public function applyLimitAndOffset(int $limit, int $offset): void;

    /**
     * @return mixed[]
     */
    public function getData(): array;
}
