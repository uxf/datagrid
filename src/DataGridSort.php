<?php

declare(strict_types=1);

namespace UXF\DataGrid;

final readonly class DataGridSort
{
    public function __construct(
        public string $name,
        public DataGridSortDir $dir,
    ) {
    }
}
