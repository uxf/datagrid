<?php

declare(strict_types=1);

namespace UXF\DataGrid;

use Symfony\Component\Config\Definition\Configurator\DefinitionConfigurator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;
use UXF\DataGrid\DependencyInjection\DataGridTypePass;

final class UXFDataGridBundle extends AbstractBundle
{
    public function configure(DefinitionConfigurator $definition): void
    {
        $definition->rootNode()
            ->children()
                ->arrayNode('gen')
                    ->addDefaultsIfNotSet()
                        ->children()
                            ->booleanNode('schema_with_hidden_columns')->defaultValue(false)->end()
                            ->arrayNode('areas')
                                ->useAttributeAsKey('name')
                                ->arrayPrototype()
                                    ->children()
                                        ->scalarNode('allowed')->defaultValue(null)->end()
                                        ->scalarNode('disabled')->defaultValue(null)->end()
                                        ->scalarNode('destination')->isRequired()->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end();
    }

    /**
     * @param array<mixed> $config
     */
    public function loadExtension(array $config, ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        $container->import(__DIR__ . '/../config/services.php');

        $parameters = $container->parameters();
        $parameters->set('uxf_data_grid.gen.areas', $config['gen']['areas']);
        $parameters->set('uxf_data_grid.gen.schema_with_hidden_columns', $config['gen']['schema_with_hidden_columns']);

        $builder->registerForAutoconfiguration(DataGridTypeWithoutCustomDataSource::class)
            ->addTag('uxf.datagrid.type');
    }

    public function build(ContainerBuilder $container): void
    {
        parent::build($container);

        $container->addCompilerPass(new DataGridTypePass());
    }
}
