<?php

declare(strict_types=1);

namespace UXF\DataGrid\GQL\Input;

use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Input as LegacyInput;
use UXF\DataGrid\DataGridSortDir;
use UXF\GraphQL\Attribute\Input;

#[LegacyInput]
#[Input(generateFake: false)]
final readonly class DataGridInput
{
    /**
     * @param DataGridFilterInput[] $f
     */
    public function __construct(
        #[Field] public array $f = [],
        #[Field] public ?string $tab = null,
        #[Field] public ?string $search = null,
        #[Field] public ?DataGridSortInput $s = null,
        #[Field] public ?bool $withTabCounts = null,
        #[Field] public ?int $page = null,
        #[Field] public ?int $perPage = null,
        /** @deprecated use $s */
        #[Field] public ?string $sort = null,
        /** @deprecated use $s */
        #[Field] public ?DataGridSortDir $dir = null,
    ) {
    }
}
