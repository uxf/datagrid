<?php

declare(strict_types=1);

namespace UXF\DataGrid\GQL\Input;

use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Input as LegacyInput;
use UXF\GraphQL\Attribute\Input;
use UXF\GraphQL\Type\Json;

#[LegacyInput]
#[Input(generateFake: false)]
final readonly class DataGridFilterInput
{
    public function __construct(
        #[Field] public string $name,
        #[Field] public ?Json $value = null,
        #[Field] public ?string $op = null,
    ) {
    }
}
