<?php

declare(strict_types=1);

namespace UXF\DataGrid\GQL\Type;

use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type as LegacyType;
use UXF\GraphQL\Attribute\Type;
use UXF\GraphQL\Type\Json;

#[LegacyType(name: 'DataGrid')]
#[Type('DataGrid')]
final readonly class DataGridType
{
    /**
     * @param Json[] $result
     */
    public function __construct(
        #[Field] public array $result,
        #[Field] public int $count,
        #[Field] public int $totalCount,
        #[Field] public ?Json $tabCounts,
    ) {
    }
}
