<?php

declare(strict_types=1);

namespace UXF\DataGrid;

use Closure;
use LogicException;
use Nette\Utils\Arrays;
use UXF\DataGrid\Column\Column;
use UXF\DataGrid\Column\LegacyColumnHelper;
use UXF\DataGrid\DataSource\DataSource;
use UXF\DataGrid\Filter\Filter;
use UXF\DataGrid\Filter\FilterWithAutocomplete;
use UXF\DataGrid\Filter\FilterWithOptions;
use UXF\DataGrid\Schema\ColumnSchema;
use UXF\DataGrid\Schema\DataGridSchema;
use UXF\DataGrid\Schema\FilterOption;
use UXF\DataGrid\Schema\FilterSchema;
use UXF\DataGrid\Schema\TabSchema;
use UXF\DataGrid\Tab\Tab;

/**
 * @template T
 */
final class DataGridBuilder
{
    /**
     * @param DataSource<T>|null $dataSource
     * @param array<string, Column<T>> $columns
     * @param array<string, Filter<mixed>> $filters
     * @param array<string, Tab> $tabs
     * @phpstan-param Closure(mixed $source, string $term): mixed|null $fullTextFilterCallback
     * @phpstan-param Closure(T $item): mixed|null $rowContentCallback
     * @phpstan-param Closure(T $item): mixed|null $rowExportCallback
     */
    public function __construct(
        private readonly string $name,
        private ?DataSource $dataSource = null,
        private array $columns = [],
        private array $filters = [],
        private array $tabs = [],
        private ?Closure $fullTextFilterCallback = null,
        private ?Closure $rowContentCallback = null,
        private ?Closure $rowExportCallback = null,
        private string $defaultSortColumnName = 'id',
        private DataGridSortDir $defaultSortColumnDirection = DataGridSortDir::ASC,
        private int $defaultPerPage = 10,
    ) {
    }

    public function clear(): void
    {
        $this->dataSource = null;
        $this->columns = [];
        $this->filters = [];
        $this->tabs = [];
        $this->fullTextFilterCallback = null;
        $this->rowContentCallback = null;
        $this->rowExportCallback = null;
        $this->defaultSortColumnName = 'id';
        $this->defaultSortColumnDirection = DataGridSortDir::ASC;
        $this->defaultPerPage = 10;
    }

    /**
     * @param DataSource<T>|null $dataSource
     * @return self<T>
     */
    public function setDataSource(?DataSource $dataSource): self
    {
        $this->dataSource = $dataSource;
        return $this;
    }

    /**
     * @return DataSource<T>|null
     */
    public function getDataSource(): ?DataSource
    {
        return $this->dataSource;
    }

    /**
     * @internal
     * @return array<string, Column<T>>
     */
    public function getColumns(bool $sorted = true): array
    {
        if ($sorted) {
            // TODO Bee resolver problem with embedded order
            uasort($this->columns, static function (Column $cA, Column $cb) {
                return $cA->getOrder() <=> $cb->getOrder();
            });
        }

        return $this->columns;
    }

    /**
     * @return Column<T>
     */
    public function getColumn(string $name): Column
    {
        return $this->columns[$name];
    }

    /**
     * @return self<T>
     */
    public function removeColumn(string $name, bool $withFilter = true): self
    {
        unset($this->columns[$name]);
        if ($withFilter) {
            $this->removeFilter($name);
        }
        return $this;
    }

    /**
     * @internal
     * @return array<string, Filter<mixed>>
     */
    public function getFilters(): array
    {
        return $this->filters;
    }

    /**
     * @deprecated
     * @return Filter<mixed>
     */
    public function getFilter(string $name): Filter
    {
        return $this->filters[$name];
    }

    /**
     * @return self<T>
     */
    public function removeFilter(string $name): self
    {
        unset($this->filters[$name]);
        return $this;
    }

    /**
     * @deprecated use addColumn
     * @return Column<T>
     */
    public function addBasicColumn(string $name, string $label, ?string $contentColumnPath = null): Column
    {
        return $this->addColumn(new Column($name, $label, $contentColumnPath));
    }

    /**
     * @deprecated use addColumn
     * @return Column<T>
     */
    public function addToOneColumn(string $name, string $label, ?string $contentColumnPath = null): Column
    {
        return $this->addColumn(LegacyColumnHelper::createToOne($name, $label, $contentColumnPath));
    }

    /**
     * @deprecated use addColumn
     * @return Column<T>
     */
    public function addToManyColumn(string $name, string $label, ?string $contentColumnPath = null): Column
    {
        return $this->addColumn(LegacyColumnHelper::createToMany($name, $label, $contentColumnPath));
    }

    /**
     * @param Column<T> $column
     * @return Column<T>
     */
    public function addColumn(Column $column, ?string $insertBeforeColumn = null): Column
    {
        if ($insertBeforeColumn !== null) {
            if (!isset($this->columns[$insertBeforeColumn])) {
                throw new LogicException("Column '$insertBeforeColumn' does not exist");
            }

            Arrays::insertBefore($this->columns, $insertBeforeColumn, [
                $column->getName() => $column,
            ]);

            return $column;
        }

        return $this->columns[$column->getName()] = $column;
    }

    /**
     * @template TF
     * @param Filter<TF> $filter
     * @return Filter<TF>
     */
    public function addFilter(Filter $filter, ?string $insertBeforeFilter = null): Filter
    {
        if ($insertBeforeFilter !== null) {
            if (!isset($this->filters[$insertBeforeFilter])) {
                throw new LogicException("Filter '$insertBeforeFilter' does not exist");
            }

            Arrays::insertBefore($this->filters, $insertBeforeFilter, [
                $filter->getName() => $filter,
            ]);

            return $filter;
        }

        return $this->filters[$filter->getName()] = $filter;
    }

    /**
     * @internal
     * @return array<string, Tab>
     */
    public function getTabs(): array
    {
        return $this->tabs;
    }

    /**
     * @return self<T>
     */
    public function removeTab(string $name): self
    {
        unset($this->tabs[$name]);
        return $this;
    }

    public function addTab(Tab $tab, ?string $insertBeforeTab = null): Tab
    {
        if ($insertBeforeTab !== null) {
            if (!isset($this->tabs[$insertBeforeTab])) {
                throw new LogicException("Tab '$insertBeforeTab' does not exist");
            }

            Arrays::insertBefore($this->tabs, $insertBeforeTab, [
                $tab->name => $tab,
            ]);

            return $tab;
        }

        return $this->tabs[$tab->name] = $tab;
    }

    /**
     * @phpstan-return Closure(mixed $source, string $term): mixed|null
     */
    public function getFullTextFilterCallback(): ?Closure
    {
        return $this->fullTextFilterCallback;
    }

    /**
     * @phpstan-param Closure(mixed $source, string $term): mixed|null $fullTextFilterCallback
     */
    public function setFullTextFilterCallback(?Closure $fullTextFilterCallback): void
    {
        $this->fullTextFilterCallback = $fullTextFilterCallback;
    }

    /**
     * @phpstan-return Closure(T $item): mixed|null
     */
    public function getRowContentCallback(): ?Closure
    {
        return $this->rowContentCallback;
    }

    /**
     * @phpstan-param Closure(T $item): mixed|null $rowContentCallback
     */
    public function setRowContentCallback(?Closure $rowContentCallback): void
    {
        $this->rowContentCallback = $rowContentCallback;
    }

    /**
     * @phpstan-return Closure(T $item): mixed|null
     */
    public function getRowExportCallback(): ?Closure
    {
        return $this->rowExportCallback;
    }

    /**
     * @phpstan-param Closure(T $item): mixed|null $rowExportCallback
     */
    public function setRowExportCallback(?Closure $rowExportCallback): void
    {
        $this->rowExportCallback = $rowExportCallback;
    }

    /**
     * @return self<T>
     */
    public function setDefaultSort(string $name, DataGridSortDir $direction): self
    {
        $this->defaultSortColumnName = $name;
        $this->defaultSortColumnDirection = $direction;
        return $this;
    }

    /**
     * @return self<T>
     */
    public function setDefaultPerPage(int $defaultPerPage): self
    {
        $this->defaultPerPage = $defaultPerPage;
        return $this;
    }

    public function getDefaultSortColumnName(): string
    {
        return $this->defaultSortColumnName;
    }

    public function getDefaultSortColumnDirection(): DataGridSortDir
    {
        return $this->defaultSortColumnDirection;
    }

    public function getDefaultPerPage(): int
    {
        return $this->defaultPerPage;
    }

    /**
     * @deprecated
     * @phpstan-param Closure(T $item): mixed|null $callback
     */
    public function setColumnContentCallback(?Closure $callback): void
    {
        trigger_error('Use setRowContentCallback', E_USER_DEPRECATED);
        $this->setRowContentCallback($callback);
    }

    /**
     * @deprecated
     * @phpstan-param Closure(T $item): mixed|null $callback
     */
    public function setColumnExportCallback(?Closure $callback): void
    {
        trigger_error('Use setRowExportCallback', E_USER_DEPRECATED);
        $this->setRowExportCallback($callback);
    }

    public function createSchema(bool $withHiddenColumns): DataGridSchema
    {
        $columns = $this->columns;

        if (!$withHiddenColumns) {
            $columns = array_filter($columns, static fn (Column $column) => !$column->isHidden());
        }

        usort($columns, static function (Column $cA, Column $cB) {
            return $cA->getOrder() <=> $cB->getOrder();
        });

        $filters = $this->filters;
        usort($filters, static function (Filter $fA, Filter $fB) {
            return $fA->getOrder() <=> $fB->getOrder();
        });

        return new DataGridSchema(
            name: $this->name,
            columns: array_map(static fn (Column $column) => new ColumnSchema(
                name: $column->getName(),
                label: $column->getLabel(),
                sort: $column->isSort(),
                hidden: $column->isHidden(),
                type: $column->getType(),
                config: $column->getConfig(),
            ), $columns),
            filters: array_map(static fn (Filter $filter) => new FilterSchema(
                name: $filter->getName(),
                label: $filter->getLabel(),
                type: $filter->getType(),
                autocomplete: $filter instanceof FilterWithAutocomplete ? $filter->getAutocomplete() : null,
                options: self::buildOptions($filter),
            ), $filters),
            tabs: array_map(static fn (Tab $tab) => new TabSchema(
                name: $tab->name,
                label: $tab->label,
                icon: $tab->icon,
                s: $tab->s,
            ), array_values($this->tabs)),
            fullText: $this->fullTextFilterCallback !== null,
            s: new DataGridSort(
                name: $this->defaultSortColumnName,
                dir: $this->defaultSortColumnDirection,
            ),
            perPage: $this->defaultPerPage,
            sort: $this->defaultSortColumnName,
            dir: $this->defaultSortColumnDirection,
        );
    }

    /**
     * @param Filter<mixed> $filter
     * @return FilterOption[]|null
     */
    private static function buildOptions(Filter $filter): ?array
    {
        if (!$filter instanceof FilterWithOptions) {
            return null;
        }

        return $filter->getOptions();
    }
}
