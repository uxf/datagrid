<?php

declare(strict_types=1);

namespace UXF\DataGrid;

use UXF\DataGrid\DataSource\DataSource;

/**
 * @template T
 * @template-extends DataGridTypeWithoutCustomDataSource<T>
 */
interface DataGridType extends DataGridTypeWithoutCustomDataSource
{
    /**
     * @param mixed[] $options
     * @return DataSource<T>
     */
    public function getDataSource(array $options = []): DataSource;
}
