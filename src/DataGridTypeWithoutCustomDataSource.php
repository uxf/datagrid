<?php

declare(strict_types=1);

namespace UXF\DataGrid;

/**
 * @template T
 */
interface DataGridTypeWithoutCustomDataSource
{
    /**
     * @param DataGridBuilder<T> $builder
     * @param mixed[] $options
     */
    public function buildGrid(DataGridBuilder $builder, array $options = []): void;

    public static function getName(): string;
}
