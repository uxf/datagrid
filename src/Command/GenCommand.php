<?php

declare(strict_types=1);

namespace UXF\DataGrid\Command;

use Exception;
use Nette\Utils\Json;
use Psr\Container\ContainerInterface;
use RuntimeException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use UXF\Core\Contract\Metadata\MetadataLoader;
use UXF\Core\Utils\Lst;
use UXF\DataGrid\Config\GridGenConfig;
use UXF\DataGrid\DataGridFactory;

#[AsCommand(name: 'uxf:grid-gen')]
final class GenCommand extends Command
{
    public function __construct(
        private readonly ContainerInterface $container,
        private readonly DataGridFactory $dataGridFactory,
        private readonly GridGenConfig $config,
        private readonly ?MetadataLoader $metadataLoader,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $fs = new Filesystem();
        foreach ($this->config->areas as $config) {
            $fs->mkdir($config['destination']);

            $gridNames = Lst::from($this->getGridNames());
            if ($config['allowed'] !== null) {
                $gridNames = $gridNames->filter(fn (string $n) => preg_match($config['allowed'], $n) === 1);
            }
            if ($config['disabled'] !== null) {
                $gridNames = $gridNames->filter(fn (string $n) => preg_match($config['disabled'], $n) !== 1);
            }

            $gridNames = $gridNames->getValues();
            foreach ($gridNames as $gridName) {
                $content = Json::encode($this->dataGridFactory->createSchema($gridName), pretty: true);
                file_put_contents("{$config['destination']}/{$gridName}.json", $content);
            }

            $this->cleanUp($gridNames);
        }

        return 0;
    }

    /**
     * @return string[]
     */
    private function getGridNames(): array
    {
        $gridNames = [];

        $metadatas = $this->metadataLoader?->getMetadata() ?? [];
        foreach (array_keys($metadatas) as $entityAlias) {
            try {
                $this->dataGridFactory->createDataGrid($entityAlias);
                $gridNames[] = $entityAlias;
            } catch (Exception) {
                // do nothing
            }
        }

        // check container
        if (!$this->container instanceof Container) {
            throw new RuntimeException('Invalid container');
        }

        foreach ($this->container->getServiceIds() as $serviceId) {
            if (str_starts_with($serviceId, 'datagrid.')) {
                $gridNames[] = str_replace('datagrid.', '', $serviceId);
            }
        }

        return array_unique($gridNames);
    }

    /**
     * @param string[] $gridNames
     */
    private function cleanUp(array $gridNames): void
    {
        $fs = new Filesystem();
        foreach ($this->config->areas as $config) {
            if (!$fs->exists($config['destination'])) {
                continue;
            }

            $directoriesToRemove = (new Finder())->files()->in($config['destination']);

            foreach ($directoriesToRemove as $item) {
                if (!in_array($item->getBasename('.json'), $gridNames, true)) {
                    $fs->remove($item->getRealPath());
                }
            }
        }
    }
}
