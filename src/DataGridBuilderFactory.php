<?php

declare(strict_types=1);

namespace UXF\DataGrid;

/**
 * @template T
 */
interface DataGridBuilderFactory
{
    /**
     * @return DataGridBuilder<T>|null
     */
    public function tryCreateBuilder(string $name): ?DataGridBuilder;
}
