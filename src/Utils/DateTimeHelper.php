<?php

declare(strict_types=1);

namespace UXF\DataGrid\Utils;

use Exception;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;

final readonly class DateTimeHelper
{
    /**
     * @return ($time is true ? DateTime : Date)
     */
    public static function stringToDateTime(?string $date, bool $time): Date|DateTime|null
    {
        if ($date === null || $date === '') {
            return null;
        }

        try {
            return $time ? new DateTime($date) : new Date($date);
        } catch (Exception) {
            return null;
        }
    }

    /**
     * @param array<string, mixed> $data
     * @return ($time is true ? array{DateTime|null, DateTime|null} : array{Date|null, Date|null})
     */
    public static function convertRange(array $data, bool $time): array
    {
        return [
            self::stringToDateTime($data['from'] ?? null, $time),
            self::stringToDateTime($data['to'] ?? null, $time),
        ];
    }
}
