<?php

declare(strict_types=1);

namespace UXF\DataGrid\Utils;

final readonly class PropertyPathHelper
{
    public static function normalize(mixed $item, string $propertyPath): string
    {
        if (is_array($item)) {
            $propertyPath = '[' . implode('][', explode('.', $propertyPath)) . ']';
        } else {
            $propertyPath = implode('?.', explode('.', $propertyPath));
        }

        return $propertyPath;
    }
}
