<?php

declare(strict_types=1);

namespace UXF\DataGrid\Utils;

use Doctrine\ORM\QueryBuilder;

final readonly class DataGridMagicHelper
{
    /**
     * @param literal-string $joinString
     */
    public static function trySafelyApplyJoin(QueryBuilder $queryBuilder, string $joinString): QueryBuilder
    {
        [, $newAlias] = explode('.', $joinString);

        if (in_array($newAlias, $queryBuilder->getAllAliases(), true)) {
            return $queryBuilder;
        }

        return $queryBuilder->leftJoin($joinString, $newAlias);
    }
}
