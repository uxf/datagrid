<?php

declare(strict_types=1);

namespace UXF\DataGrid\Column;

use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use UXF\DataGrid\Utils\PropertyPathHelper;

/**
 * @deprecated use Column
 *
 * @template T
 * @template-extends Column<T>
 */
final class ToOneColumn extends Column
{
    protected string $type = 'toOne';

    public function getContent(mixed $item, PropertyAccessorInterface $propertyAccessor): mixed
    {
        if (is_callable($this->customContentCallback)) {
            return call_user_func($this->customContentCallback, $item);
        }

        $path = PropertyPathHelper::normalize($item, $this->name);
        if ($propertyAccessor->getValue($item, $path) === null) {
            return null;
        }

        return [
            'id' => $propertyAccessor->getValue($item, PropertyPathHelper::normalize($item, "$this->name.id")),
            'label' => $propertyAccessor->getValue($item, PropertyPathHelper::normalize($item, $this->contentColumnPath)),
        ];
    }

    public function getExportContent(mixed $item, PropertyAccessorInterface $propertyAccessor): mixed
    {
        if (is_callable($this->customExportCallback)) {
            return call_user_func($this->customExportCallback, $item);
        }

        if ($propertyAccessor->getValue($item, $this->name) === null) {
            return null;
        }

        return $propertyAccessor->getValue($item, $this->contentColumnPath);
    }
}
