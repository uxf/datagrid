<?php

declare(strict_types=1);

namespace UXF\DataGrid\Column;

use Doctrine\Common\Collections\Collection;
use RuntimeException;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

/**
 * @deprecated use Column
 *
 * @template T
 * @template-extends Column<T>
 */
final class ToManyColumn extends Column
{
    protected string $type = 'toMany';
    protected bool $export = false;

    public function getContent(mixed $item, PropertyAccessorInterface $propertyAccessor): mixed
    {
        if (is_callable($this->customContentCallback)) {
            return call_user_func($this->customContentCallback, $item);
        }

        $items = $propertyAccessor->getValue($item, $this->name);
        $items = $items instanceof Collection ? $items->toArray() : $items;

        $contentColumnPath = str_starts_with($this->contentColumnPath, $this->name)
            ? substr($this->contentColumnPath, strlen($this->name) + 1)
            : $this->contentColumnPath;

        return array_map(static fn (mixed $collectionItem) => [
            'id' => $propertyAccessor->getValue($collectionItem, 'id'),
            'label' => $propertyAccessor->getValue($collectionItem, $contentColumnPath),
        ], $items);
    }

    public function getExportContent(mixed $item, PropertyAccessorInterface $propertyAccessor): mixed
    {
        if (is_callable($this->customExportCallback)) {
            return call_user_func($this->customExportCallback, $item);
        }

        throw new RuntimeException('Default export To Many is not supported');
    }
}
