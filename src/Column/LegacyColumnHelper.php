<?php

declare(strict_types=1);

namespace UXF\DataGrid\Column;

use Doctrine\Common\Collections\Collection;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use UXF\DataGrid\Utils\PropertyPathHelper;

/**
 * @internal
 */
final class LegacyColumnHelper
{
    /**
     * @param string[]|null $sortColumnPaths
     * @return Column<mixed>
     */
    public static function createToMany(
        string $name,
        string $label,
        ?string $contentColumnPath = null,
        ?array $sortColumnPaths = null,
    ): Column {
        $column = new Column(
            name: $name,
            label: $label,
            contentColumnPath: $contentColumnPath,
            sortColumnPaths: $sortColumnPaths,
        );

        $column->setType('toMany');
        $column->setExport(false);

        $column->setCustomContentCallback(function (mixed $item) use ($column) {
            $propertyAccessor = new PropertyAccessor();

            $name = $column->getName();
            $path = $column->getContentColumnPath();
            $items = $propertyAccessor->getValue($item, $name);
            $items = $items instanceof Collection ? $items->toArray() : $items;

            $contentColumnPath = str_starts_with($path, $name)
                ? substr($path, strlen($name) + 1)
                : $path;

            return array_map(static fn (mixed $collectionItem) => [
                'id' => $propertyAccessor->getValue($collectionItem, 'id'),
                'label' => $propertyAccessor->getValue($collectionItem, $contentColumnPath),
            ], $items);
        });

        return $column;
    }

    /**
     * @param string[]|null $sortColumnPaths
     * @return Column<mixed>
     */
    public static function createToOne(
        string $name,
        string $label,
        ?string $contentColumnPath = null,
        ?array $sortColumnPaths = null,
    ): Column {
        $column = new Column(
            name: $name,
            label: $label,
            contentColumnPath: $contentColumnPath,
            sortColumnPaths: $sortColumnPaths,
        );

        $column->setType('toOne');

        $column->setCustomContentCallback(function (mixed $item) use ($column) {
            $propertyAccessor = new PropertyAccessor();

            $name = $column->getName();

            $path = PropertyPathHelper::normalize($item, $name);
            if ($propertyAccessor->getValue($item, $path) === null) {
                return null;
            }

            return [
                'id' => $propertyAccessor->getValue($item, PropertyPathHelper::normalize($item, "$name.id")),
                'label' => $propertyAccessor->getValue($item, PropertyPathHelper::normalize($item, $column->getContentColumnPath())),
            ];
        });

        $column->setCustomExportCallback(function (mixed $item) use ($column) {
            $propertyAccessor = new PropertyAccessor();

            if ($propertyAccessor->getValue($item, $column->getName()) === null) {
                return null;
            }

            return $propertyAccessor->getValue($item, $column->getContentColumnPath());
        });

        return $column;
    }
}
