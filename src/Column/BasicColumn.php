<?php

declare(strict_types=1);

namespace UXF\DataGrid\Column;

/**
 * @deprecated use Column
 *
 * @template T
 * @template-extends Column<T>
 */
final class BasicColumn extends Column
{
}
