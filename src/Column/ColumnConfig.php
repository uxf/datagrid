<?php

declare(strict_types=1);

namespace UXF\DataGrid\Column;

use JsonSerializable;
use stdClass;
use UXF\Core\Http\Request\NotSet;

final readonly class ColumnConfig implements JsonSerializable
{
    public function __construct(
        public int|string|NotSet $width = new NotSet(),
        public bool|NotSet $isHidden = new NotSet(),
    ) {
    }

    public function jsonSerialize(): stdClass
    {
        if ($this->width instanceof NotSet && $this->isHidden instanceof NotSet) {
            return new stdClass();
        }

        $result = [];
        if (!$this->width instanceof NotSet) {
            $result['width'] = $this->width;
        }
        if (!$this->isHidden instanceof NotSet) {
            $result['isHidden'] = $this->isHidden;
        }

        return (object) $result;
    }
}
