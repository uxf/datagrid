<?php

declare(strict_types=1);

namespace UXF\DataGrid\Exception;

use Exception;
use Throwable;

/**
 * @author Jakub Janata <jakubjanata@gmail.com>
 */
final class DataGridException extends Exception
{
    public function __construct(string $message, ?Throwable $previous = null)
    {
        parent::__construct($message, 0, $previous);
    }
}
