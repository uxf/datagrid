<?php

declare(strict_types=1);

namespace UXF\DataGrid\Config;

final readonly class GridGenConfig
{
    /**
     * @param array<string, array{allowed: string|null, disabled: string|null, destination: string}> $areas
     */
    public function __construct(
        public array $areas,
    ) {
    }

    /**
     * @param array<mixed> $config
     */
    public static function create(array $config): self
    {
        return new self($config);
    }
}
