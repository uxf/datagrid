<?php

declare(strict_types=1);

namespace UXF\DataGrid\Filter;

use UXF\DataGrid\Schema\FilterOption;

/**
 * @extends Filter<array<string|int>>
 */
final class MultiSelectFilter extends Filter implements FilterWithOptions
{
    /** @var FilterOption[] */
    private array $options;

    /**
     * @param FilterOption[] $options
     */
    public function __construct(string $name, string $label, array $options, ?string $columnPath = null)
    {
        parent::__construct($name, $label, $columnPath);
        $this->options = $options;
    }

    protected function getDefaultType(): string
    {
        return 'multiSelect';
    }

    /**
     * @return array<string|int>
     */
    public function mapFilterValue(mixed $value): array
    {
        return array_map(static fn (mixed $item) => is_array($item) ? ($item['id'] ?? 0) : $item, $value);
    }

    /**
     * @return FilterOption[]
     */
    public function getOptions(): array
    {
        return $this->options;
    }
}
