<?php

declare(strict_types=1);

namespace UXF\DataGrid\Filter;

use UXF\Core\Type\Date;
use UXF\DataGrid\Utils\DateTimeHelper;

/**
 * @extends Filter<array{Date|null, Date|null}>
 * @implements RangeFilter<array{Date|null, Date|null}>
 */
final class DateRangeFilter extends Filter implements RangeFilter
{
    public function __construct(string $name, string $label, ?string $columnPath = null)
    {
        parent::__construct($name, $label, $columnPath);
    }

    protected function getDefaultType(): string
    {
        return 'date';
    }

    /**
     * @return array{Date|null, Date|null}
     */
    public function mapFilterValue(mixed $value): array
    {
        return DateTimeHelper::convertRange($value, false);
    }
}
