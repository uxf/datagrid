<?php

declare(strict_types=1);

namespace UXF\DataGrid\Filter;

interface FilterWithAutocomplete
{
    public function getAutocomplete(): string;
}
