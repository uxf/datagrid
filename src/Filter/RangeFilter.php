<?php

declare(strict_types=1);

namespace UXF\DataGrid\Filter;

/**
 * @template T of array
 */
interface RangeFilter
{
    /**
     * @return T
     */
    public function mapFilterValue(mixed $value): array;
}
