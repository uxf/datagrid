<?php

declare(strict_types=1);

namespace UXF\DataGrid\Filter;

/**
 * @extends Filter<array<string|int>>
 */
final class EntityMultiSelectFilter extends Filter implements FilterWithAutocomplete
{
    public function __construct(string $name, string $label, private readonly string $autocomplete)
    {
        parent::__construct($name, $label, "$name.id");
    }

    protected function getDefaultType(): string
    {
        return 'entityMultiSelect';
    }

    /**
     * @return array<string|int>
     */
    public function mapFilterValue(mixed $value): array
    {
        return array_map(static fn (mixed $item) => is_array($item) ? ($item['id'] ?? 0) : $item, $value);
    }

    public function getAutocomplete(): string
    {
        return $this->autocomplete;
    }
}
