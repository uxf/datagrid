<?php

declare(strict_types=1);

namespace UXF\DataGrid\Filter;

use UXF\Core\Type\DateTime;
use UXF\DataGrid\Utils\DateTimeHelper;

/**
 * @extends Filter<array{DateTime|null, DateTime|null}>
 * @implements RangeFilter<array{DateTime|null, DateTime|null}>
 */
final class DateTimeRangeFilter extends Filter implements RangeFilter
{
    public function __construct(string $name, string $label, ?string $columnPath = null)
    {
        parent::__construct($name, $label, $columnPath);
    }

    protected function getDefaultType(): string
    {
        return 'datetime';
    }

    /**
     * @return array{DateTime|null, DateTime|null}
     */
    public function mapFilterValue(mixed $value): array
    {
        return DateTimeHelper::convertRange($value, true);
    }
}
