<?php

declare(strict_types=1);

namespace UXF\DataGrid\Filter;

use Closure;
use Doctrine\ORM\QueryBuilder;

/**
 * @template-covariant T
 */
abstract class Filter
{
    protected ?string $type = null;
    protected string $name;
    protected string $label;
    protected string $columnPath;
    protected int $order = 0;
    /** @phpstan-var Closure(QueryBuilder $qb, T $value, ?string $operator): mixed|null */
    protected ?Closure $qbCallback = null;
    protected bool $embedded = false;
    protected mixed $defaultValue = null;

    public function __construct(string $name, string $label, ?string $columnPath = null)
    {
        $this->name = $name;
        $this->label = $label;
        $this->columnPath = $columnPath ?? $name;
    }

    public function getType(): string
    {
        return $this->type ?? $this->getDefaultType();
    }

    public function setType(?string $type): void
    {
        $this->type = $type;
    }

    abstract protected function getDefaultType(): string;

    /**
     * @return T
     */
    abstract public function mapFilterValue(mixed $value): mixed;

    public function getName(): string
    {
        return $this->name;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getColumnPath(): string
    {
        return $this->columnPath;
    }

    /**
     * @deprecated will be removed
     */
    public function getOrder(): int
    {
        return $this->order;
    }

    /**
     * @deprecated will be removed
     * @return self<T>
     */
    public function setOrder(int $order): self
    {
        $this->order = $order;
        return $this;
    }

    /**
     * @phpstan-return Closure(QueryBuilder $qb, T $value, ?string $operator): mixed|null
     */
    public function getQbCallback(): ?Closure
    {
        return $this->qbCallback;
    }

    /**
     * @phpstan-param Closure(QueryBuilder $qb, T $value, ?string $operator): mixed $qbCallback
     */
    public function setQbCallback(Closure $qbCallback): void
    {
        $this->qbCallback = $qbCallback;
    }

    /**
     * @deprecated
     */
    public function isEmbedded(): bool
    {
        return $this->embedded;
    }

    /**
     * @deprecated use self::setQbCallback()
     * @return self<T>
     */
    public function setEmbedded(bool $embedded): self
    {
        $this->embedded = $embedded;
        return $this;
    }

    public function getDefaultValue(): mixed
    {
        return $this->defaultValue;
    }

    public function setDefaultValue(mixed $defaultValue): void
    {
        $this->defaultValue = $defaultValue;
    }
}
