<?php

declare(strict_types=1);

namespace UXF\DataGrid\Filter;

use UXF\DataGrid\Schema\FilterOption;

interface FilterWithOptions
{
    /**
     * @return FilterOption[]
     */
    public function getOptions(): array;
}
