<?php

declare(strict_types=1);

namespace UXF\DataGrid\Filter;

/**
 * @deprecated use RangeIntFilter or RangeDecimalFilter
 * @extends Filter<array{mixed, mixed}>
 */
final class IntervalFilter extends Filter
{
    protected function getDefaultType(): string
    {
        return 'interval';
    }

    /**
     * @return array{mixed,mixed}
     */
    public function mapFilterValue(mixed $value): array
    {
        return [
            is_numeric($value['from'] ?? null) ? $value['from'] : null,
            is_numeric($value['to'] ?? null) ? $value['to'] : null,
        ];
    }
}
