<?php

declare(strict_types=1);

namespace UXF\DataGrid\Filter;

/**
 * @extends Filter<array{int|null, int|null}>
 * @implements RangeFilter<array{int|null, int|null}>
 */
final class IntRangeFilter extends Filter implements RangeFilter
{
    protected function getDefaultType(): string
    {
        return 'interval'; // TODO intRange
    }

    /**
     * @return array{int|null, int|null}
     */
    public function mapFilterValue(mixed $value): array
    {
        return [
            is_numeric($value['from'] ?? null) ? (int) $value['from'] : null,
            is_numeric($value['to'] ?? null) ? (int) $value['to'] : null,
        ];
    }
}
