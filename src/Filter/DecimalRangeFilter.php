<?php

declare(strict_types=1);

namespace UXF\DataGrid\Filter;

use UXF\Core\Type\Decimal;

/**
 * @extends Filter<array{Decimal|null, Decimal|null}>
 * @implements RangeFilter<array{Decimal|null, Decimal|null}>
 */
final class DecimalRangeFilter extends Filter implements RangeFilter
{
    protected function getDefaultType(): string
    {
        return 'interval'; // TODO decimalRange
    }

    /**
     * @return array{Decimal|null, Decimal|null}
     */
    public function mapFilterValue(mixed $value): array
    {
        return [
            is_numeric($value['from'] ?? null) ? Decimal::of($value['from']) : null,
            is_numeric($value['to'] ?? null) ? Decimal::of($value['to']) : null,
        ];
    }
}
