<?php

declare(strict_types=1);

namespace UXF\DataGrid\Filter;

use BackedEnum;
use ReflectionEnum;
use UXF\CMS\Attribute\CmsLabel;
use UXF\Core\Attribute\Label;
use UXF\DataGrid\Schema\FilterOption;

/**
 * @template T of BackedEnum
 * @extends Filter<T>
 */
final class EnumFilter extends Filter implements FilterWithOptions
{
    /** @var class-string<T> $enumClass */
    private string $enumClass;

    /**
     * @param class-string<T> $enumClass
     */
    public function __construct(string $name, string $label, string $enumClass, ?string $columnPath = null)
    {
        parent::__construct($name, $label, $columnPath);
        $this->enumClass = $enumClass;
    }

    protected function getDefaultType(): string
    {
        return 'select';
    }

    public function mapFilterValue(mixed $value): BackedEnum
    {
        $isInt = (new ReflectionEnum($this->enumClass))->getBackingType()?->getName() === 'int';
        $value = is_array($value) ? ($value['id'] ?? 0) : $value;
        $value = $isInt ? (int) $value : (string) $value;

        return $this->enumClass::from($value);
    }

    /**
     * @return FilterOption[]
     */
    public function getOptions(): array
    {
        $options = [];

        foreach ((new ReflectionEnum($this->enumClass))->getCases() as $caseRef) {
            $labelAttr = $caseRef->getAttributes(Label::class)[0] ?? $caseRef->getAttributes(CmsLabel::class)[0] ?? null;
            if ($labelAttr !== null) {
                $cmsLabel = $labelAttr->newInstance();
                $options[] = new FilterOption($caseRef->getBackingValue(), $cmsLabel->label);
            } else {
                $options[] = new FilterOption($caseRef->getBackingValue(), $caseRef->getName());
            }
        }

        return $options;
    }
}
