<?php

declare(strict_types=1);

namespace UXF\DataGrid\Filter;

use UXF\DataGrid\Schema\FilterOption;

/**
 * @extends Filter<string|int>
 */
final class SelectFilter extends Filter implements FilterWithOptions
{
    /** @var FilterOption[] */
    private array $options;

    /**
     * @param FilterOption[] $options
     */
    public function __construct(string $name, string $label, array $options, ?string $columnPath = null)
    {
        parent::__construct($name, $label, $columnPath);
        $this->options = $options;
    }

    protected function getDefaultType(): string
    {
        return 'select';
    }

    public function mapFilterValue(mixed $value): string|int
    {
        return is_array($value) ? ($value['id'] ?? 0) : $value;
    }

    /**
     * @return FilterOption[]
     */
    public function getOptions(): array
    {
        return $this->options;
    }
}
