<?php

declare(strict_types=1);

namespace UXF\DataGrid;

enum DataGridSortDir: string
{
    case ASC = 'asc';
    case DESC = 'desc';
}
