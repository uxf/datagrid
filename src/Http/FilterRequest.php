<?php

declare(strict_types=1);

namespace UXF\DataGrid\Http;

final readonly class FilterRequest
{
    public function __construct(
        public string $name,
        public mixed $value = null,
        public ?string $op = null,
    ) {
    }
}
