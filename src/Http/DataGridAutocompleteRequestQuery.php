<?php

declare(strict_types=1);

namespace UXF\DataGrid\Http;

final readonly class DataGridAutocompleteRequestQuery
{
    public function __construct(
        public string $term = '',
    ) {
    }
}
