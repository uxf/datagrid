<?php

declare(strict_types=1);

namespace UXF\DataGrid\Http;

use UXF\DataGrid\DataGridSort;
use UXF\DataGrid\DataGridSortDir;

final readonly class DataGridRequest
{
    /**
     * @param FilterRequest[] $f
     */
    public function __construct(
        public array $f = [],
        public ?string $tab = null,
        public ?string $search = null,
        public ?DataGridSort $s = null,
        public ?bool $withTabCounts = null,
        public ?int $page = null,
        public ?int $perPage = null,
        /** @deprecated use $s */
        public ?string $sort = null,
        /** @deprecated use $s */
        public ?DataGridSortDir $dir = null,
    ) {
    }
}
