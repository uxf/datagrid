<?php

declare(strict_types=1);

namespace UXF\DataGrid;

use BackedEnum;
use Closure;
use LogicException;
use ReflectionEnumBackedCase;
use UXF\CMS\Attribute\CmsLabel;
use UXF\Core\Attribute\Label;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Email;
use UXF\Core\Type\Money;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Url;
use UXF\DataGrid\Column\Column;
use UXF\DataGrid\Column\ColumnConfig;

trait ColumnTrait
{
    /**
     * @return Column<mixed>
     */
    public static function id(bool $isConfigHidden = false): Column
    {
        $column = new Column('id', 'ID');
        $column
            ->setType('id')
            ->setConfig(new ColumnConfig(isHidden: $isConfigHidden))
            ->setSort();

        return $column;
    }

    /**
     * @return Column<mixed>
     */
    public static function uuid(bool $isHidden = true): Column
    {
        $column = new Column('uuid', 'UUID');
        $column
            ->setType('uuid')
            ->setHidden($isHidden)
            ->setSort();

        return $column;
    }

    /**
     * @template T = mixed
     * @param Closure(T): (string|null)|null $fn
     * @return Column<T>
     */
    public static function string(string $name, string $label, ?Closure $fn = null): Column
    {
        $column = new Column($name, $label);
        $column
            ->setType('string')
            ->setCustomContentCallback($fn)
            ->setSort();

        return $column;
    }

    /**
     * @template T = mixed
     * @param Closure(T): (int|null)|null $fn
     * @return Column<T>
     */
    public static function int(string $name, string $label, ?Closure $fn = null): Column
    {
        $column = new Column($name, $label);
        $column
            ->setType('integer')
            ->setCustomContentCallback($fn)
            ->setSort();

        return $column;
    }

    /**
     * @template T = mixed
     * @param Closure(T): (Url|null)|null $fn
     * @return Column<T>
     */
    public static function url(string $name, string $label, ?Closure $fn = null): Column
    {
        $column = new Column($name, $label);
        $column
            ->setType('url')
            ->setCustomContentCallback($fn)
            ->setSort();

        return $column;
    }

    /**
     * @template T = mixed
     * @param Closure(T): (Money|null)|null $fn
     * @return Column<T>
     */
    public static function money(string $name, string $label, ?Closure $fn = null): Column
    {
        $column = new Column($name, $label);
        $column
            ->setType('money')
            ->setCustomContentCallback($fn)
            ->setSort()
            ->setConfig(new ColumnConfig(width: 120));

        return $column;
    }

    /**
     * @template T = mixed
     * @param Closure(T): (Phone|null)|null $fn
     * @return Column<T>
     */
    public static function phone(string $name, string $label, ?Closure $fn = null): Column
    {
        $column = new Column($name, $label);
        $column
            ->setType('phone')
            ->setCustomContentCallback($fn)
            ->setSort()
            ->setConfig(new ColumnConfig(width: 150));

        return $column;
    }

    /**
     * @template T = mixed
     * @param Closure(T): (Email|null)|null $fn
     * @return Column<T>
     */
    public static function email(string $name, string $label, ?Closure $fn = null): Column
    {
        $column = new Column($name, $label);
        $column
            ->setType('email')
            ->setCustomContentCallback($fn)
            ->setSort()
            ->setConfig(new ColumnConfig(width: 120));

        return $column;
    }

    /**
     * @template T = mixed
     * @param Closure(T): (DateTime|null)|null $fn
     * @return Column<T>
     */
    public static function dateTime(string $name, string $label, ?Closure $fn = null): Column
    {
        $column = new Column($name, $label);
        $column
            ->setType('datetime')
            ->setCustomContentCallback($fn)
            ->setSort()
            ->setConfig(new ColumnConfig(width: 150));

        return $column;
    }

    /**
     * @template T = mixed
     * @param Closure(T): (Date|null)|null $fn
     * @return Column<T>
     */
    public static function date(string $name, string $label, ?Closure $fn = null): Column
    {
        $column = new Column($name, $label);
        $column
            ->setType('date')
            ->setCustomContentCallback($fn)
            ->setSort()
            ->setConfig(new ColumnConfig(width: 100));

        return $column;
    }

    /**
     * @template T = mixed
     * @param Closure(T): (bool|null)|null $fn
     * @return Column<T>
     */
    public static function bool(string $name, string $label, ?Closure $fn = null): Column
    {
        $column = new Column($name, $label);
        $column
            ->setType('boolean')
            ->setCustomContentCallback($fn)
            ->setSort()
            ->setConfig(new ColumnConfig(width: 50));

        return $column;
    }

    /**
     * @template T
     * @param Closure(T): (BackedEnum|null) $fn
     * @return Column<T>
     */
    public static function chip(string $name, string $label, Closure $fn): Column
    {
        $column = new Column($name, $label);
        $column
            ->setType('chip')
            ->setSort()
            ->setCustomContentCallback(function ($entity) use ($fn) {
                $enum = $fn($entity);
                return $enum === null ? null : self::enumToResponse($enum);
            });

        return $column;
    }

    /**
     * @template T
     * @param Closure(T): BackedEnum[] $fn
     * @return Column<T>
     */
    public static function chips(string $name, string $label, Closure $fn): Column
    {
        $column = new Column($name, $label);
        $column
            ->setType('chips')
            ->setCustomContentCallback(fn ($entity) => array_map(self::enumToResponse(...), $fn($entity)));

        return $column;
    }

    /**
     * @return array{id: string|int, label: string, color: string|null}
     */
    private static function enumToResponse(BackedEnum $case): array
    {
        $attr = (new ReflectionEnumBackedCase($case, $case->name))->getAttributes(Label::class);
        if ($attr === []) {
            // BC
            $attr = (new ReflectionEnumBackedCase($case, $case->name))->getAttributes(CmsLabel::class);
        }

        if ($attr === []) {
            throw new LogicException('Enum values must have #[Label] attribute (' . $case::class . ').');
        }

        /** @var Label $label */
        $label = $attr[0]->newInstance();

        return [
            'id' => $case->value,
            'label' => (string) ($label->label ?? $case->value),
            'color' => $label->color,
        ];
    }
}
