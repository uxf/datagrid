default: paratest

init:
	tests/bin/console doctrine:schema:drop --force
	tests/bin/console doctrine:schema:create
	tests/bin/console doctrine:fixtures:load --append

paratest: init
	./../../vendor/bin/paratest --colors

phpunit: init
	./../../vendor/bin/phpunit
