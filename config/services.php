<?php

declare(strict_types=1);

use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use UXF\DataGrid\Command\GenCommand;
use UXF\DataGrid\Config\GridGenConfig;
use UXF\DataGrid\Controller\DataGridController;
use UXF\DataGrid\DataGridBuilderFactory;
use UXF\DataGrid\DataGridFactory;
use UXF\DataGrid\DataGridResponseProvider;
use UXF\DataGrid\Export\DataGridExporter;
use UXF\DataGrid\GQL\DataGridResponseProvider as DataGridGQLResponseProvider;
use UXF\DataGrid\SimpleDataGridBuilderFactory;
use function Symfony\Component\DependencyInjection\Loader\Configurator\param;

return static function (ContainerConfigurator $containerConfigurator): void {
    $services = $containerConfigurator->services();

    $services->alias(ContainerInterface::class, 'service_container');

    $services->set(DataGridFactory::class)
        ->arg('$schemaWithHiddenColumns', param('uxf_data_grid.gen.schema_with_hidden_columns'))
        ->autowire();

    $services->set(DataGridResponseProvider::class)->autowire();
    $services->set(DataGridGQLResponseProvider::class)->autowire();
    $services->set(DataGridExporter::class)->autowire();

    $services->set(SimpleDataGridBuilderFactory::class);

    $services->alias(DataGridBuilderFactory::class, SimpleDataGridBuilderFactory::class);

    $services->set(DataGridController::class)
        ->autowire()
        ->public();

    $services->set(GenCommand::class)
        ->autoconfigure()
        ->autowire();

    $services->set(GridGenConfig::class)
        ->factory([null, 'create'])
        ->arg('$config', param('uxf_data_grid.gen.areas'));
};
