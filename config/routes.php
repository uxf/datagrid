<?php

declare(strict_types=1);

use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;
use UXF\DataGrid\Controller\DataGridController;

return static function (RoutingConfigurator $routingConfigurator): void {
    $routingConfigurator->add('datagrid_get_schema', '/api/cms/datagrid/schema/{name}')
        ->controller([DataGridController::class, 'getSchema'])
        ->methods(['GET']);

    $routingConfigurator->add('datagrid_get_autocomplete', '/api/cms/datagrid/autocomplete/{name}')
        ->controller([DataGridController::class, 'getAutocomplete'])
        ->methods(['GET']);

    $routingConfigurator->add('datagrid_get_export', '/api/cms/datagrid/export/{name}')
        ->controller([DataGridController::class, 'getExport'])
        ->methods(['GET']);

    $routingConfigurator->add('datagrid_get_data', '/api/cms/datagrid/{name}')
        ->controller([DataGridController::class, 'getData'])
        ->methods(['GET']);
};
