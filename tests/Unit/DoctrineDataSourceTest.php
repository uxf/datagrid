<?php

declare(strict_types=1);

namespace UXF\DataGridTests\Unit;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Monolog\Test\TestCase;
use PHPUnit\Framework\Attributes\DataProvider;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Decimal;
use UXF\DataGrid\DataSource\DoctrineDataSource;
use UXF\DataGrid\Filter\CheckboxFilter;
use UXF\DataGrid\Filter\DateRangeFilter;
use UXF\DataGrid\Filter\DateTimeRangeFilter;
use UXF\DataGrid\Filter\DecimalRangeFilter;
use UXF\DataGrid\Filter\EntityMultiSelectFilter;
use UXF\DataGrid\Filter\EntitySelectFilter;
use UXF\DataGrid\Filter\EnumFilter;
use UXF\DataGrid\Filter\EnumsFilter;
use UXF\DataGrid\Filter\Filter;
use UXF\DataGrid\Filter\IntRangeFilter;
use UXF\DataGrid\Filter\MultiSelectFilter;
use UXF\DataGrid\Filter\SelectFilter;
use UXF\DataGridTests\Project\Entity\EInt;
use UXF\DataGridTests\Project\Entity\EString;
use UXF\DataGridTests\Project\Entity\User;

class DoctrineDataSourceTest extends TestCase
{
    /**
     * @param Filter<mixed> $filter
     */
    #[DataProvider('getData')]
    public function test(mixed $expected, Filter $filter, mixed $value): void
    {
        $qb = new QueryBuilder(self::createStub(EntityManagerInterface::class));
        $qb->select('x')->from(User::class, 'x');
        $dataSource = new DoctrineDataSource($qb);
        $dataSource->applyFilter($filter, $value, null);

        self::assertSame($expected, $qb->getParameters()->get(0)?->getValue());
    }

    /**
     * @param Filter<mixed> $filter
     */
    #[DataProvider('getRangeData')]
    public function testRange(mixed $expected, Filter $filter, mixed $value): void
    {
        $qb = new QueryBuilder(self::createStub(EntityManagerInterface::class));
        $qb->select('x')->from(User::class, 'x');
        $dataSource = new DoctrineDataSource($qb);
        $dataSource->applyFilter($filter, $value, null);

        self::assertEquals($expected[0], $qb->getParameters()->get(0)?->getValue());
        self::assertEquals($expected[1], $qb->getParameters()->get(1)?->getValue());
    }

    /**
     * @return iterable<mixed>
     */
    public static function getData(): iterable
    {
        yield 'SelectFilter' => [
            'expected' => '1',
            'filter' => new SelectFilter('name', '', []),
            'value' => '1',
        ];
        yield 'SelectFilter array' => [
            'expected' => '1',
            'filter' => new SelectFilter('name', '', []),
            'value' => [
                'id' => '1',
            ],
        ];

        yield 'MultiSelectFilter' => [
            'expected' => ['1'],
            'filter' => new MultiSelectFilter('name', '', []),
            'value' => ['1'],
        ];
        yield 'MultiSelectFilter array' => [
            'expected' => ['1'],
            'filter' => new MultiSelectFilter('name', '', []),
            'value' => [[
                'id' => '1',
            ]],
        ];

        yield 'EntitySelectFilter' => [
            'expected' => '1',
            'filter' => new EntitySelectFilter('name', '', ''),
            'value' => '1',
        ];
        yield 'EntitySelectFilter array' => [
            'expected' => '1',
            'filter' => new EntitySelectFilter('name', '', ''),
            'value' => [
                'id' => '1',
            ],
        ];

        yield 'EntityMultiSelectFilter' => [
            'expected' => ['1'],
            'filter' => new EntityMultiSelectFilter('name', '', ''),
            'value' => ['1'],
        ];
        yield 'EntityMultiSelectFilter array' => [
            'expected' => ['1'],
            'filter' => new EntityMultiSelectFilter('name', '', ''),
            'value' => [[
                'id' => '1',
            ]],
        ];

        yield 'CheckboxFilter string' => [
            'expected' => true,
            'filter' => new CheckboxFilter('name', ''),
            'value' => '1',
        ];
        yield 'CheckboxFilter string false' => [
            'expected' => false,
            'filter' => new CheckboxFilter('name', ''),
            'value' => '0',
        ];
        yield 'CheckboxFilter bool false' => [
            'expected' => false,
            'filter' => new CheckboxFilter('name', ''),
            'value' => false,
        ];
        yield 'CheckboxFilter bool true' => [
            'expected' => true,
            'filter' => new CheckboxFilter('name', ''),
            'value' => true,
        ];

        yield 'EnumFilter int' => [
            'expected' => EInt::X,
            'filter' => new EnumFilter('name', '', EInt::class),
            'value' => '1',
        ];
        yield 'EnumFilter int array' => [
            'expected' => EInt::X,
            'filter' => new EnumFilter('name', '', EInt::class),
            'value' => [
                'id' => '1',
            ],
        ];
        yield 'EnumFilter string' => [
            'expected' => EString::X,
            'filter' => new EnumFilter('name', '', EString::class),
            'value' => 'X',
        ];
        yield 'EnumFilter string array' => [
            'expected' => EString::X,
            'filter' => new EnumFilter('name', '', EString::class),
            'value' => [
                'id' => 'X',
            ],
        ];

        yield 'EnumsFilter int' => [
            'expected' => [EInt::X],
            'filter' => new EnumsFilter('name', '', EInt::class),
            'value' => ['1'],
        ];
        yield 'EnumsFilter int Array' => [
            'expected' => [EInt::X],
            'filter' => new EnumsFilter('name', '', EInt::class),
            'value' => [[
                'id' => '1',
            ]],
        ];
        yield 'EnumsFilter string' => [
            'expected' => [EString::X],
            'filter' => new EnumsFilter('name', '', EString::class),
            'value' => ['X'],
        ];
        yield 'EnumsFilter string Array' => [
            'expected' => [EString::X],
            'filter' => new EnumsFilter('name', '', EString::class),
            'value' => [[
                'id' => 'X',
            ]],
        ];
    }

    /**
     * @return iterable<mixed>
     */
    public static function getRangeData(): iterable
    {
        yield 'DateRangeFilter' => [
            'expected' => [
                new Date('2000-01-02'),
                new Date('2000-01-03'),
            ],
            'filter' => new DateRangeFilter('name', ''),
            'value' => [
                'from' => '2000-01-02',
                'to' => '2000-01-03',
            ],
        ];
        yield 'DateRangeFilter empty' => [
            'expected' => [null, null],
            'filter' => new DateRangeFilter('name', ''),
            'value' => [],
        ];

        yield 'DateTimeRangeFilter' => [
            'expected' => [
                new DateTime('2000-01-02 10:00'),
                new DateTime('2000-01-03 11:00'),
            ],
            'filter' => new DateTimeRangeFilter('name', ''),
            'value' => [
                'from' => '2000-01-02 10:00',
                'to' => '2000-01-03 11:00',
            ],
        ];
        yield 'DateTimeRangeFilter empty' => [
            'expected' => [null, null],
            'filter' => new DateTimeRangeFilter('name', ''),
            'value' => [],
        ];

        yield 'DecimalRangeFilter' => [
            'expected' => [
                Decimal::of('1.1'),
                Decimal::of('2.2'),
            ],
            'filter' => new DecimalRangeFilter('name', ''),
            'value' => [
                'from' => '1.1',
                'to' => '2.2',
            ],
        ];
        yield 'DecimalRangeFilter empty' => [
            'expected' => [null, null],
            'filter' => new DecimalRangeFilter('name', ''),
            'value' => [],
        ];

        yield 'IntRangeFilter' => [
            'expected' => [1, 2],
            'filter' => new IntRangeFilter('name', ''),
            'value' => [
                'from' => '1',
                'to' => '2',
            ],
        ];
        yield 'IntRangeFilter float' => [
            'expected' => [1, 2],
            'filter' => new IntRangeFilter('name', ''),
            'value' => [
                'from' => '1.1',
                'to' => '2.2',
            ],
        ];
        yield 'IntRangeFilter empty' => [
            'expected' => [null, null],
            'filter' => new IntRangeFilter('name', ''),
            'value' => [],
        ];
    }
}
