<?php

declare(strict_types=1);

namespace UXF\DataGridTests\Unit;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use UXF\DataGrid\Utils\DataGridMagicHelper;

class DataGridMagicHelperTest extends TestCase
{
    /**
     * @param literal-string[] $joinStrings
     */
    #[DataProvider('getData')]
    public function test(string $expected, array $joinStrings): void
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $queryBuilder = new QueryBuilder($em);
        $queryBuilder->from('root', 'r');

        foreach ($joinStrings as $joinString) {
            DataGridMagicHelper::trySafelyApplyJoin($queryBuilder, $joinString);
        }

        self::assertSame($expected, $queryBuilder->getDQL());
    }

    /**
     * @return array<mixed>
     */
    public static function getData(): array
    {
        return [
            [
                'SELECT FROM root r LEFT JOIN r.profile profile',
                ['r.profile'],
            ],
            [
                'SELECT FROM root r LEFT JOIN r.profile profile LEFT JOIN profile.user user',
                ['r.profile', 'profile.user'],
            ],
            [
                'SELECT FROM root r LEFT JOIN r.profile profile LEFT JOIN profile.user user LEFT JOIN user.role role',
                ['r.profile', 'profile.user', 'user.role'],
            ],
            [
                'SELECT FROM root r LEFT JOIN r.profile profile',
                ['r.profile', 'r.profile'],
            ],
            [
                'SELECT FROM root r LEFT JOIN r.profile profile LEFT JOIN profile.user user',
                ['r.profile',  'profile.user', 'profile.user'],
            ],
            [
                'SELECT FROM root r LEFT JOIN r.profile profile LEFT JOIN profile.user user LEFT JOIN profile.user2 user2',
                ['r.profile',  'profile.user', 'profile.user2'],
            ],
        ];
    }
}
