<?php

declare(strict_types=1);

namespace UXF\DataGridTests\Unit;

use Monolog\Test\TestCase;
use UXF\Core\Http\Request\NotSet;
use UXF\DataGrid\Column\Column;

class ColumnTest extends TestCase
{
    public function testReplaceConfig(): void
    {
        $column = new Column('', '');

        $config = $column->replaceConfig()->getConfig();
        self::assertInstanceOf(NotSet::class, $config->width);
        self::assertInstanceOf(NotSet::class, $config->isHidden);

        $config = $column->replaceConfig(width: 1)->getConfig();
        self::assertSame(1, $config->width);
        self::assertInstanceOf(NotSet::class, $config->isHidden);

        $config = $column->replaceConfig(isHidden: true)->getConfig();
        self::assertSame(1, $config->width);
        self::assertTrue($config->isHidden);

        $config = $column->replaceConfig(width: '10', isHidden: new NotSet())->getConfig();
        self::assertSame('10', $config->width);
        self::assertInstanceOf(NotSet::class, $config->isHidden);
    }
}
