<?php

declare(strict_types=1);

namespace UXF\DataGridTests\GQL;

use Symfony\Component\HttpKernel\KernelInterface;
use UXF\Core\Test\WebTestCase;
use UXF\DataGrid\DataGridSortDir;
use UXF\DataGrid\GQL\Input\DataGridFilterInput;
use UXF\DataGrid\GQL\Input\DataGridInput;
use UXF\DataGrid\GQL\Input\DataGridSortInput;
use UXF\GraphQL\Type\Json;

class SmokeGQLTest extends WebTestCase
{
    public function test(): void
    {
        $client = self::createClient();

        $client->post('/graphql', [
            'query' => file_get_contents(__DIR__ . '/DataGridQuery.graphql'),
            'variables' => [
                'name' => 'user',
                'input' => new DataGridInput(
                    f: [new DataGridFilterInput('name', new Json('man'))],
                    s: new DataGridSortInput(
                        name: 'name',
                        dir: DataGridSortDir::DESC,
                    ),
                ),
            ],
        ]);
        self::assertResponseIsSuccessful();
        self::assertSame([
            'data' => [
                'dataGrid' => [
                    'result' => [
                        [
                            'id' => 1,
                            'name' => 'Superman',
                            'role' => [
                                'id' => 1,
                                'label' => 'ADMIN',
                            ],
                            'fake' => 'Superman CUSTOM',
                        ],
                    ],
                    'count' => 1,
                    'totalCount' => 1,
                    'tabCounts' => null,
                ],
            ],
        ], $client->getResponseData());
    }

    public function testWithTabCounts(): void
    {
        $client = self::createClient();

        $client->post('/graphql', [
            'query' => file_get_contents(__DIR__ . '/DataGridQuery.graphql'),
            'variables' => [
                'name' => 'user',
                'input' => new DataGridInput(
                    f: [new DataGridFilterInput('name', new Json('man'))],
                    s: new DataGridSortInput(
                        name: 'name',
                        dir: DataGridSortDir::DESC,
                    ),
                    withTabCounts: true,
                ),
            ],
        ]);
        self::assertResponseIsSuccessful();
        self::assertSame([
            'data' => [
                'dataGrid' => [
                    'result' => [
                        [
                            'id' => 1,
                            'name' => 'Superman',
                            'role' => [
                                'id' => 1,
                                'label' => 'ADMIN',
                            ],
                            'fake' => 'Superman CUSTOM',
                        ],
                    ],
                    'count' => 1,
                    'totalCount' => 1,
                    'tabCounts' => [
                        'all' => 1,
                        'one' => 1,
                    ],
                ],
            ],
        ], $client->getResponseData());
    }

    /**
     * @param array<mixed> $options
     */
    protected static function createKernel(array $options = []): KernelInterface
    {
        return new KernelGQL('test', true);
    }
}
