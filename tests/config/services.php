<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use UXF\Core\Test\Client;
use UXF\DataGridTests\DataFixtures\TestFixtures;
use UXF\DataGridTests\Project\Grid\HelicopterArrayGridType;
use UXF\DataGridTests\Project\Grid\PlaneArrayGridType;
use UXF\DataGridTests\Project\Grid\RoleGridType;
use UXF\DataGridTests\Project\Grid\UserGridType;
use UXF\DataGridTests\Project\Grid\WithoutPaginatorGridType;

return static function (ContainerConfigurator $containerConfigurator): void {
    $services = $containerConfigurator->services();

    $services->defaults()
        ->public()
        ->autoconfigure()
        ->autowire();

    $services->set(UserGridType::class);
    $services->set(PlaneArrayGridType::class);
    $services->set(HelicopterArrayGridType::class);
    $services->set(WithoutPaginatorGridType::class);
    $services->set(RoleGridType::class);

    $services->set(TestFixtures::class);

    $services->set(Client::class);

    $services->alias('test.client', Client::class);

    $containerConfigurator->extension('uxf_data_grid', [
        'gen' => [
            'schema_with_hidden_columns' => true,
            'areas' => [
                'test' => [
                    'destination' => __DIR__ . '/../../var/gen',
                ],
            ],
        ],
    ]);

    $containerConfigurator->extension('framework', [
        'secret' => 'test-secret',
        'test' => true,
        'validation' => [
            'email_validation_mode' => 'html5',
        ],
        'http_method_override' => false,
    ]);

    $containerConfigurator->extension('doctrine', [
        'dbal' => [
            'url' => 'sqlite:///%kernel.project_dir%/var/db.sqlite',
        ],
        'orm' => [
            'naming_strategy' => 'doctrine.orm.naming_strategy.underscore_number_aware',
            'auto_mapping' => true,
            'report_fields_where_declared' => true,
            'enable_lazy_ghost_objects' => true,
            'mappings' => [
                'test' => [
                    'type' => 'attribute',
                    'dir' => '%kernel.project_dir%/tests/Project',
                    'prefix' => 'UXF\DataGridTests\Project',
                ],
            ],
        ],
    ]);
};
