<?php

declare(strict_types=1);

namespace UXF\DataGridTests\Project\Dto;

use UXF\Core\Type\Date;

final readonly class HelicopterDto
{
    public function __construct(
        public int $id,
        public string $name,
        public Date $date,
        public ?SelectDto $role,
        public ?NestedDto $test,
    ) {
    }
}
