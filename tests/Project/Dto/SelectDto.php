<?php

declare(strict_types=1);

namespace UXF\DataGridTests\Project\Dto;

final readonly class SelectDto
{
    public function __construct(
        public int $id,
        public string $name,
    ) {
    }
}
