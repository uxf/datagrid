<?php

declare(strict_types=1);

namespace UXF\DataGridTests\Project\Entity;

enum EInt: int
{
    case X = 1;
    case Y = 2;
}
