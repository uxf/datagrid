<?php

declare(strict_types=1);

namespace UXF\DataGridTests\Project\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class User
{
    #[ORM\Column, ORM\Id, ORM\GeneratedValue]
    private int $id = 0;

    #[ORM\Column]
    private string $name;

    #[ORM\ManyToOne, ORM\JoinColumn(nullable: false)]
    private Role $role;

    public function __construct(string $name, Role $role)
    {
        $this->name = $name;
        $this->role = $role;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getRole(): Role
    {
        return $this->role;
    }
}
