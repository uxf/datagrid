<?php

declare(strict_types=1);

namespace UXF\DataGridTests\Project\Grid;

use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use UXF\DataGrid\Column\Column;
use UXF\DataGrid\DataGridBuilder;
use UXF\DataGrid\DataGridType;
use UXF\DataGrid\DataSource\ArrayDataSource;
use UXF\DataGrid\DataSource\DataSource;
use UXF\DataGrid\Filter\DateRangeFilter;
use UXF\DataGrid\Filter\EntitySelectFilter;
use UXF\DataGrid\Filter\SelectFilter;
use UXF\DataGrid\Filter\StringFilter;
use UXF\DataGrid\Schema\FilterOption;

/**
 * @template-implements DataGridType<array<mixed>>
 */
final readonly class PlaneArrayGridType implements DataGridType
{
    public function __construct(private PropertyAccessorInterface $propertyAccessor)
    {
    }

    /**
     * @inheritDoc
     */
    public function buildGrid(DataGridBuilder $builder, array $options = []): void
    {
        $builder->addColumn(new Column('id', 'ID'));
        $builder->addColumn(new Column('name', 'Name'));
        $builder->addColumn(new Column('date', 'Date'));
        $builder->addColumn(new Column('role', 'Role', 'role.name'))
            ->setType('toOne')
            ->setCustomContentCallback(fn (array $item) => [
                'id' => $item['role']['id'],
                'label' => $item['role']['name'],
            ]);
        $builder->addColumn(new Column('test', 'Test'))
            ->setCustomContentCallback(fn (array $item) => []);

        $builder->addFilter(new StringFilter('name', 'User name'));
        $builder->addFilter(new DateRangeFilter('date', 'Date'));
        $builder->addFilter(new EntitySelectFilter('role', 'Role name', 'role'));
        $builder->addFilter(new SelectFilter('test', 'Fake', [
            new FilterOption('X', 'L1'),
            new FilterOption('Y', 'L2'),
        ], 'test.nested'));
    }

    public static function getName(): string
    {
        return 'plane';
    }

    /**
     * @inheritDoc
     */
    public function getDataSource(array $options = []): DataSource
    {
        return new ArrayDataSource(
            fn () => [
                [
                    'id' => 1,
                    'name' => 'Wow',
                    'date' => '2024-01-01',
                    'role' => [
                        'id' => 2,
                        'name' => 'OK',
                    ],
                    'test' => [
                        'nested' => 'Y',
                    ],
                ],
                [
                    'id' => 2,
                    'name' => '?',
                    'date' => '2020-01-01',
                    'role' => null,
                    'test' => null,
                ],
            ],
            $this->propertyAccessor,
        );
    }
}
