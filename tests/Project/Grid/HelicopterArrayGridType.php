<?php

declare(strict_types=1);

namespace UXF\DataGridTests\Project\Grid;

use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use UXF\Core\Type\Date;
use UXF\DataGrid\Column\Column;
use UXF\DataGrid\DataGridBuilder;
use UXF\DataGrid\DataGridType;
use UXF\DataGrid\DataSource\ArrayDataSource;
use UXF\DataGrid\DataSource\DataSource;
use UXF\DataGrid\Filter\DateRangeFilter;
use UXF\DataGrid\Filter\EntitySelectFilter;
use UXF\DataGrid\Filter\SelectFilter;
use UXF\DataGrid\Filter\StringFilter;
use UXF\DataGrid\Schema\FilterOption;
use UXF\DataGridTests\Project\Dto\HelicopterDto;
use UXF\DataGridTests\Project\Dto\NestedDto;
use UXF\DataGridTests\Project\Dto\SelectDto;

/**
 * @template-implements DataGridType<HelicopterDto>
 */
final readonly class HelicopterArrayGridType implements DataGridType
{
    public function __construct(private PropertyAccessorInterface $propertyAccessor)
    {
    }

    /**
     * @inheritDoc
     */
    public function buildGrid(DataGridBuilder $builder, array $options = []): void
    {
        $builder->addColumn(new Column('id', 'ID'));
        $builder->addColumn(new Column('name', 'Name'));
        $builder->addColumn(new Column('date', 'Date'));
        $builder->addColumn(new Column('role', 'Role', 'role.name'))
            ->setType('toOne')
            ->setCustomContentCallback(fn (HelicopterDto $dto) => $dto->role === null ? null : [
                'id' => $dto->role->id,
                'label' => $dto->role->name,
            ]);
        $builder->addColumn(new Column('test', 'Test'));

        $builder->addFilter(new StringFilter('name', 'User name'));
        $builder->addFilter(new DateRangeFilter('date', 'Date'));
        $builder->addFilter(new EntitySelectFilter('role', 'Role name', 'role'));
        $builder->addFilter(new SelectFilter('test', 'Fake', [
            new FilterOption('X', 'L1'),
            new FilterOption('Y', 'L2'),
        ], 'test.nested'));
    }

    public static function getName(): string
    {
        return 'helicopter';
    }

    /**
     * @inheritDoc
     */
    public function getDataSource(array $options = []): DataSource
    {
        return new ArrayDataSource(
            fn () => [
                new HelicopterDto(1, 'Wow', new Date('2024-01-01'), new SelectDto(2, 'OK'), new NestedDto('Y')),
                new HelicopterDto(2, '?', new Date('2020-01-01'), null, null),
            ],
            $this->propertyAccessor,
        );
    }
}
