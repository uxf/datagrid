<?php

declare(strict_types=1);

namespace UXF\DataGridTests\Project\Grid;

use UXF\DataGrid\FilterTrait;

final readonly class FilterFake
{
    use FilterTrait;
}
