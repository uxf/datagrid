<?php

declare(strict_types=1);

namespace UXF\DataGridTests\Project\Grid;

use Doctrine\ORM\EntityManagerInterface;
use UXF\DataGrid\Column\Column;
use UXF\DataGrid\DataGridBuilder;
use UXF\DataGrid\DataGridType;
use UXF\DataGrid\DataSource\DataSource;
use UXF\DataGrid\DataSource\DoctrineDataSource;
use UXF\DataGridTests\Project\Entity\Role;

/**
 * @template-implements DataGridType<Role>
 */
class RoleGridType implements DataGridType
{
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }

    /**
     * @inheritDoc
     */
    public function buildGrid(DataGridBuilder $builder, array $options = []): void
    {
        $builder->addColumn(new Column('id', 'ID'));
        $builder->addColumn(new Column('name', 'Name'));
    }

    /**
     * @param array<mixed> $options
     */
    public function getDataSource(array $options = []): DataSource
    {
        $qb = $this->entityManager->createQueryBuilder()->select('r')->from(Role::class, 'r');
        return new DoctrineDataSource($qb);
    }

    public static function getName(): string
    {
        return 'role';
    }
}
