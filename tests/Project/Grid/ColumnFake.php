<?php

declare(strict_types=1);

namespace UXF\DataGridTests\Project\Grid;

use UXF\DataGrid\ColumnTrait;

final readonly class ColumnFake
{
    use ColumnTrait;
}
