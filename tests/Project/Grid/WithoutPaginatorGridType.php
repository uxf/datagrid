<?php

declare(strict_types=1);

namespace UXF\DataGridTests\Project\Grid;

use Doctrine\ORM\EntityManagerInterface;
use UXF\DataGrid\Column\Column;
use UXF\DataGrid\Column\ColumnConfig;
use UXF\DataGrid\DataGridBuilder;
use UXF\DataGrid\DataGridTypeWithoutCustomDataSource;
use UXF\DataGrid\DataSource\DoctrineDataSource;
use UXF\DataGrid\Filter\StringFilter;
use UXF\DataGridTests\Project\Entity\User;

/**
 * @template-implements DataGridTypeWithoutCustomDataSource<User>
 */
final readonly class WithoutPaginatorGridType implements DataGridTypeWithoutCustomDataSource
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    /**
     * @inheritDoc
     */
    public function buildGrid(DataGridBuilder $builder, array $options = []): void
    {
        $qb = $this->entityManager->createQueryBuilder()->select('u')->from(User::class, 'u');

        $builder->setDataSource(new DoctrineDataSource($qb, true, true));

        $builder->addColumn(new Column('id', 'ID'))->setConfig(new ColumnConfig(100));
        $builder->addColumn(new Column('name', 'Name'))->setConfig(new ColumnConfig(isHidden: true));

        $builder->addFilter(new StringFilter('name', 'User name'));
    }

    public static function getName(): string
    {
        return 'user2';
    }
}
