<?php

declare(strict_types=1);

namespace UXF\DataGridTests\DataFixtures;

use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use UXF\DataGridTests\Project\Entity\Role;
use UXF\DataGridTests\Project\Entity\User;

class TestFixtures implements ORMFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $role = new Role('ADMIN');
        $manager->persist($role);

        $user = new User('Superman', $role);
        $manager->persist($user);

        $manager->flush();
    }
}
